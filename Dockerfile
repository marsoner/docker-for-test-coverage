FROM ubuntu:bionic

LABEL maintainer="lukas.marsoner@gmail.com"

# Update ubuntu
RUN apt-get update -qq

# Install needed programs
RUN apt-get install -qq curl gnupg2 lcov git python3.8 python3-pip
RUN apt-get update && apt-get upgrade -y

# Install dart
RUN sh -c 'curl https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -' 
RUN sh -c 'curl https://storage.googleapis.com/download.dartlang.org/linux/debian/dart_stable.list > /etc/apt/sources.list.d/dart_stable.list'
RUN apt-get update -qq
RUN apt-get install -qq dart
ENV PATH="/usr/lib/dart/bin:$PATH"
ENV PATH="/root/.pub-cache/bin:$PATH"
RUN pub global activate coverage
RUN pip3 install anybadge firebase-admin